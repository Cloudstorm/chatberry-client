/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatberryclient;

/**
 *
 * @author Paul
 */
public class KeepAlive implements Runnable
{

    private Client client;

    KeepAlive(Client client)
    {
        this.client = client;
    }

    @Override
    public void run()
    {
        while (!Thread.currentThread().isInterrupted())
        {
            client.alive = false;

            try
            {
                Thread.sleep(3000);
            } catch (InterruptedException ex)
            {
                 //It's not a problem if our sleep gets interrupted. Can happen
                //Logger.getLogger(KeepAlive.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (client.alive == false)
            {
                client.connected = false;
                System.err.println("ERROR: Connection to server lost!");
                break;
            }
        }
    }

}
