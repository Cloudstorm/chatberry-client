/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatberryclient;

import java.lang.Thread.UncaughtExceptionHandler;

/**
 *
 * @author Paul
 */
public class LastRescue implements UncaughtExceptionHandler
{

    @Override
    public void uncaughtException(Thread t, Throwable e)
    {
        System.err.println("UNCAUGHT EXCEPTION: " + t.getName() + " Throwable: " + e.getMessage()
                + "\nCause: " + e.getCause());
        e.printStackTrace();
    }

}
