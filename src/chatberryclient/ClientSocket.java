/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatberryclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Paul
 */
public class ClientSocket
{

    private Socket ClientSock;
    private PrintWriter Writer;
    private BufferedReader Reader;
    private final String Hostname;
    private final int Port;

    ClientSocket(String Hostname, int Port)
    {
        this.Hostname = Hostname;
        this.Port = Port;
    }

    public boolean connect()
    {
        try
        {
            ClientSock = new Socket(Hostname, Port);
            ClientSock.setSoTimeout(50);
            Writer = new PrintWriter(ClientSock.getOutputStream(), true);
            Reader = new BufferedReader(new InputStreamReader(ClientSock.getInputStream()));

        } catch (IOException | IllegalArgumentException ex) //IOException
        {
            //Logger.getLogger(ClientSocket.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

        return true;
    }

    public boolean disconnect()
    {
        if (connected())
        {
            try
            {
                ClientSock.close();
            } catch (IOException ex)
            {
                Logger.getLogger(ClientSocket.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
            return true;
        } else
        {
            return false;
        }
    }

    public boolean connected()
    {
        return !ClientSock.isClosed() && ClientSock.isConnected();
    }

    public boolean send(String Msg)
    {
        if (connected())
        {
            Writer.println(Msg);
            if (Writer.checkError())
            {
                return false;
            }
            Writer.flush();

            return true;
        } else
        {
            return false;
        }
    }

    public String read()
    {
        if (connected())
        {
            try
            {
                if (Reader.ready())
                {
                    return Reader.readLine();
                }
            } catch (SocketTimeoutException ex)
            {
                //Logger.getLogger(WorkSocket.class.getName()).log(Level.SEVERE, null, ex);
                System.err.println("ERROR: Read-timeout");
            } catch (IOException ex)
            {
                //Logger.getLogger(WorkSocket.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
}
