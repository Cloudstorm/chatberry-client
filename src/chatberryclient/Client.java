/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatberryclient;

/**
 *
 * @author Paul
 */
public class Client
{

    private final String Hostname;
    private final int Port;
    private Thread MListener;
    private Thread KeepAlive;
    private final ClientSocket ClientSock;
    private final String Nick;
    boolean alive;
    boolean connected;

    Client(String Hostname, int Port, String Nick)
    {
        this.Hostname = Hostname;
        this.Port = Port;
        ClientSock = new ClientSocket(Hostname, this.Port);
        this.Nick = Nick;
        alive = false; //Our alive flag for the KeepAlive thread to work with
        connected = false;
    }

    public boolean connect()
    {
        if (ClientSock.connect())
        {
            alive = true; //Let's do this before we start the threads to prevent problems with keepalive
            connected = true;

            MListener = new Thread(new MessageListener(this, ClientSock));
            KeepAlive = new Thread(new KeepAlive(this));

            MListener.setDaemon(true);
            KeepAlive.setDaemon(true);

            MListener.start();
            KeepAlive.start();

            ClientSock.send("/" + Nick);
        } else
        {
            return false;
        }

        return true;
    }

    public boolean disconnect()
    {
        if (ClientSock.disconnect())
        {
            MListener.interrupt();
            KeepAlive.interrupt();
            alive = false;
            connected = false;
            
        } else
        {
            return false;
        }

        return true;
    }

    public boolean connected()
    {
        return ClientSock.connected() && connected; //Not sure about this one yet, might override our nice alive-check
    }

    public boolean send(String Msg)
    {
        if (Msg.indexOf('/') == 0)
        {
            if (Msg.toLowerCase().substring(1).equals("exit"))
            {
                ClientSock.send("!*|" + "EXIT");
            } else
            {
                Msg = "@*|" + Msg; //CMD
            }
        } else
        {
            if (!Msg.isEmpty())
            {
                Msg = "+*|" + Msg; //Msg
            }
        }
        return ClientSock.send(Msg); //Return info if message could be sent
    }

    public boolean sendRaw(String Msg)
    {
        return ClientSock.send(Msg);
    }

}
