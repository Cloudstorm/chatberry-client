/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatberryclient;

import java.util.regex.Pattern;

/**
 *
 * @author Paul
 */
public class MessageListener implements Runnable
{

    private final ClientSocket ClientSock;
    private final Client client;
    private String Msg;

    MessageListener(Client client, ClientSocket ClientSock)
    {
        this.client = client;
        this.ClientSock = ClientSock;
    }

    @Override
    public void run()
    {
        Thread.currentThread().setUncaughtExceptionHandler(new LastRescue());
        String[] FMsg;

        while (!Thread.currentThread().isInterrupted())
        {
            Msg = ClientSock.read();
            if (Msg != null)
            {
                //Split username + message!
                FMsg = (Msg).split(Pattern.quote("|"));

                switch (analyzeMsg(FMsg[0])) //Only analyze part before username, rest is msg
                {
                    case 1:
                        command(FMsg[0].substring(1)); //Why would be receive commands as a client? (Remove me?)
                        break;
                    case 2:
                        if (FMsg.length > 1) //Only show message if it's not empty
                        {
                            System.out.println(FMsg[0].substring(1) + ": " + FMsg[1]);
                        }
                        break;
                    case 3:
                        if ("EXIT".equals(FMsg[1]))
                        {
                            client.disconnect();
                        } else
                        {
                            if ("ALIVE".equals(FMsg[1])) //Alive signal. If server asks if alive, reply
                            {
                                client.sendRaw("!*|ALIVE");
                                client.alive = true;
                            }
                        }
                        break;
                    default:
                        System.err.println("ERROR: Invalid message recieved!");
                        System.out.println("DEBUG:");
                        System.out.println(Msg);
                }
            }

            try
            {
                Thread.sleep(50); //Don't max out the cpu, wait a bit after each loop
            } catch (InterruptedException ex)
            {
                //Don't show this, doesn't matter. This only happens on application-end anyway.
                //Logger.getLogger(MessageListener.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    int analyzeMsg(String Msg)
    {
        if (Msg.indexOf('@') == 0)
        {
            return 1; //CMD
        } else
        {
            if (Msg.indexOf('+') == 0)
            {
                return 2; //MSG
            } else
            {
                if (Msg.indexOf('!') == 0)
                {
                    return 3; //SIG
                } else
                {
                    return -1; //Other or Err
                }
            }
        }
    }

    private void command(String CMD) //No use for this yet
    {
        switch (CMD)
        {
        }
    }
}
