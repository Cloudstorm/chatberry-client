/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatberryclient;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 *
 * @author Paul
 *
 */
public class ChatBerryClient
{

    public static final String CONFIGPATH = "ChatBerryClient.properties";
    public static final String LOGPATH = "ChatBerryClient.log";
    private static int port;
    private static String host;
    private static String nick;

    /**
     * @param args the command line arguments
     */
    private static Client client;
    
    @SuppressWarnings("empty-statement")
    public static void main(String[] args)
    {
        Thread.currentThread().setUncaughtExceptionHandler(new LastRescue());
        Scanner userInput = new Scanner(System.in);
        boolean restart = true;
        boolean ready = false;
        boolean ignoreConf = false;
        String choice;
        
        while (restart)
        {
            System.out.println("Welcome to ChatBerry [Client]!");
            
            do
            {
                System.out.print("[S]tart\n[C]onfiguration\n[E]xit\n>");
                
                choice = userInput.nextLine();
                
                switch (choice.toLowerCase())
                {
                    case "s":
                        if (ignoreConf)
                        {
                            ready = true;
                        } else
                        {
                            if (getSettings())
                            {
                                System.out.println("Configuration loaded!");
                                ready = true;
                            } else
                            {
                                System.err.println("ERROR: Could not load configuration-file '" + CONFIGPATH + "'! Please configure!");
                            }
                        }
                        break;
                    case "c":
                        if (putSettings())
                        {
                            System.out.println("Configuration saved!");
                            ignoreConf = false;
                        } else
                        {
                            System.err.println("ERROR: Could not save configuration-file. Settings won't survive past this session.");
                            ignoreConf = true;
                        }
                        break;
                    case "e":
                        System.exit(0);
                        break;
                    default:
                        System.out.println("Invalid input!\n");
                        break;
                }
            } while (!ready);
            
            if (connect(host, port, nick))
            {
                System.out.println("Connected! You may send chat-messages now.");
                while (chat(nick));
            }
            System.out.println("[E]xit or [R]estart?");
            restart = userInput.nextLine().equalsIgnoreCase("r");
        }
        System.out.println("Stopping ChatBerry . . .");
    }
    
    static boolean chat(String NickName)
    {
        Scanner UserInput = new Scanner(System.in);
        String UserInputS;
        
        System.out.println(">");
        UserInputS = UserInput.nextLine();
        
        if (client.connected())
        {
            if (client.send(UserInputS) == false)
            {
                System.err.println("ERROR: Could not send message!");
                return false;
            }
        } else
        {
            return false;
        }
        
        return true;
    }
    
    static String[] getConnectionAddress()
    {
        Scanner UserInput = new Scanner(System.in);
        String UserInputS;
        String[] ConnectionAddress = new String[2];
        
        UserInputS = UserInput.nextLine();
        if (!"".equals(UserInputS))
        {
            ConnectionAddress = (UserInputS).split(Pattern.quote(":"));
        }
        
        return ConnectionAddress;
    }
    
    static boolean connect(String Hostname, int Port, String NickName)
    {
        boolean Connected = false;
        int TryCount = 0;
        client = new Client(Hostname, Port, NickName);
        
        while (!Connected)
        {
            if (TryCount >= 4)
            {
                return false;
            } else
            {
                Connected = true;
                TryCount++;
                System.out.println("Connection-attempt [" + TryCount + "/4]");
                if (client.connect())
                {
                    System.out.println("Connected to " + Hostname + "!");
                } else
                {
                    Connected = false;
                    System.err.println("Error: Could not connect to server!");
                    try
                    {
                        Thread.sleep(3000);
                        
                    } catch (InterruptedException ex)
                    {
                        Logger.getLogger(ChatBerryClient.class.getName()).log(Level.SEVERE, null, ex);
                        return false;
                    }
                }
            }
        }
        
        return true;
    }
    
    private static boolean getSettings()
    {
        Properties prop = new Properties();
        BufferedInputStream fileIn;
        
        try
        {
            fileIn = new BufferedInputStream(new FileInputStream(CONFIGPATH));
            prop.load(fileIn);
            fileIn.close();
            
        } catch (FileNotFoundException ex)
        {
            //System.err.println("ERROR: Could not open '" + CONFIGPATH + "'!"); No need to show this message twice (see above)
            return false;
        } catch (IOException ex)
        {
            Logger.getLogger(ChatBerryClient.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
        host = prop.getProperty("host");
        port = Integer.parseInt(prop.getProperty("port"));
        nick = prop.getProperty("nick");
        
        return true;
    }
    
    private static boolean putSettings()
    {
        Scanner UserInput = new Scanner(System.in);
        boolean valid = false;
        FileOutputStream fileOut;
        Properties prop;
        String[] ConnectionAddress;
        
        System.out.println("\nConnection-Details: [Address:port]");
        while (!valid)
        {
            System.out.print(">");
            ConnectionAddress = getConnectionAddress();
            try
            {
                port = Integer.parseInt(ConnectionAddress[1]);
                host = ConnectionAddress[0];
                valid = true;
            } catch (ArrayIndexOutOfBoundsException | NumberFormatException ex)
            {
                valid = false;
                System.out.println("Invalid input, please try again!");
            }
        }
        
        System.out.print("Nickname>");
        nick = UserInput.nextLine();
        
        try
        {
            fileOut = new FileOutputStream(CONFIGPATH);
            prop = new Properties();
            
            prop.put("port", Integer.toString(port));
            prop.put("host", host);
            prop.put("nick", nick);
            
            prop.store(fileOut, "Configuration-file for the ChatBerry-Client.");
            fileOut.flush();
            fileOut.close();
        } catch (FileNotFoundException ex)
        {
            System.err.println("ERROR: Could not open '" + CONFIGPATH + "'!");
            return false;
        } catch (IOException ex)
        {
            Logger.getLogger(ChatBerryClient.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
        return true;
    }
}
